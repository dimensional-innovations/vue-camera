## Members

<dl>
<dt><a href="#Camera">Camera</a></dt>
<dd><p>Component that shows a stream from a camera. The stream will automatically be
resized to fit within the element.</p>
</dd>
<dt><a href="#CameraSettings">CameraSettings</a></dt>
<dd><p>Component that allows a user to changes settings on a connected camera device,
emit an event each time the user does with the current settings.</p>
</dd>
</dl>

## Functions

<dl>
<dt><a href="#useCamera">useCamera(deviceId, constraints, onError)</a> ⇒</dt>
<dd><p>Gets a media stream for the specified device once the device is connected. If any constraints are provided
or updated, automatically applies the constraints to the stream.</p>
</dd>
<dt><a href="#useCamerasList">useCamerasList()</a> ⇒</dt>
<dd><p>Gets a reactive list of devices that support video.</p>
</dd>
<dt><a href="#isDeviceConnected">isDeviceConnected(deviceId)</a> ⇒</dt>
<dd><p>Determines if the specified device is connected.</p>
</dd>
<dt><a href="#getMediaStream">getMediaStream(deviceId, constraints)</a> ⇒</dt>
<dd><p>Connects to the specified device, automatically starting the video stream.</p>
</dd>
<dt><a href="#applyConstraints">applyConstraints(mediaStream, constraints)</a> ⇒</dt>
<dd><p>Applies the given constraints to the mediaStream.</p>
</dd>
<dt><a href="#stopMediaStream">stopMediaStream(mediaStream)</a></dt>
<dd><p>Stops the given mediaStream.</p>
</dd>
<dt><a href="#toBlob">toBlob(canvas, type)</a> ⇒</dt>
<dd><p>Converts the provided canvas element into a Blob. This wraps the canvas&#39; toBlob method
in order to return a promise instead of using a callback.</p>
</dd>
<dt><a href="#imageBitmapToBlob">imageBitmapToBlob(image)</a> ⇒</dt>
<dd><p>Converts the provided image bitmap to a blob.</p>
</dd>
<dt><a href="#takePhoto">takePhoto(mediaStream)</a> ⇒</dt>
<dd><p>Attempts to capture a photo using the ImageCapture.takePhoto method. Will fallback to
using the grabFrame method if underlying device does not support the takePhoto method.</p>
</dd>
</dl>

<a name="Camera"></a>

## Camera
Component that shows a stream from a camera. The stream will automatically beresized to fit within the element.

**Kind**: global variable  
<a name="CameraSettings"></a>

## CameraSettings
Component that allows a user to changes settings on a connected camera device,emit an event each time the user does with the current settings.

**Kind**: global variable  
<a name="useCamera"></a>

## useCamera(deviceId, constraints, onError) ⇒
Gets a media stream for the specified device once the device is connected. If any constraints are providedor updated, automatically applies the constraints to the stream.

**Kind**: global function  
**Returns**: - A ref to the MediaStream. If the device cannot be found, the ref will be undefined.  

| Param | Description |
| --- | --- |
| deviceId | The unique id of the device to get the MediaStream from. |
| constraints | Constraints, if any, to apply to the stream. If this object is reactive, new constraints                      will be applied each time the object is updated. |
| onError | Callback that is invoked if there an error when retrieving the media stream. |

<a name="useCamerasList"></a>

## useCamerasList() ⇒
Gets a reactive list of devices that support video.

**Kind**: global function  
**Returns**: - The list of devices that can be used as cameras.  
<a name="isDeviceConnected"></a>

## isDeviceConnected(deviceId) ⇒
Determines if the specified device is connected.

**Kind**: global function  
**Returns**: A promise that resolves to true if the device is connected, or false otherwise.  

| Param | Description |
| --- | --- |
| deviceId | The unique id of the device. |

<a name="getMediaStream"></a>

## getMediaStream(deviceId, constraints) ⇒
Connects to the specified device, automatically starting the video stream.

**Kind**: global function  
**Returns**: - A promise that resolves with the MediaStream for the device.  

| Param | Description |
| --- | --- |
| deviceId | The unique id of the device |
| constraints | The set of constraints to apply to the mediaStream. |

<a name="applyConstraints"></a>

## applyConstraints(mediaStream, constraints) ⇒
Applies the given constraints to the mediaStream.

**Kind**: global function  
**Returns**: - A promise that resolve once the changes have been applied.  

| Param | Description |
| --- | --- |
| mediaStream | The mediaStream to modify. |
| constraints | The constraints to apply. |

<a name="stopMediaStream"></a>

## stopMediaStream(mediaStream)
Stops the given mediaStream.

**Kind**: global function  

| Param | Description |
| --- | --- |
| mediaStream | The mediaStream to stop. |

<a name="toBlob"></a>

## toBlob(canvas, type) ⇒
Converts the provided canvas element into a Blob. This wraps the canvas' toBlob methodin order to return a promise instead of using a callback.

**Kind**: global function  
**Returns**: - A promise that resolves with the resulting blob.  

| Param | Default | Description |
| --- | --- | --- |
| canvas |  | The canvas element to convert. |
| type | <code>image/png</code> | The mime type of the converted canvas. Defaults to 'image/png'. |

<a name="imageBitmapToBlob"></a>

## imageBitmapToBlob(image) ⇒
Converts the provided image bitmap to a blob.

**Kind**: global function  
**Returns**: - The blob representing the converted image.  

| Param | Description |
| --- | --- |
| image | The image to convert |

<a name="takePhoto"></a>

## takePhoto(mediaStream) ⇒
Attempts to capture a photo using the ImageCapture.takePhoto method. Will fallback tousing the grabFrame method if underlying device does not support the takePhoto method.

**Kind**: global function  
**Returns**: - A promise that resolves with the Blob of the captured image.  

| Param | Description |
| --- | --- |
| mediaStream | The mediaStream used to capture a photo. |

