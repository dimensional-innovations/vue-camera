# @dimensional-innovations/vue-camera

vue-camera builds on the [mediaDevices API](https://developer.mozilla.org/en-US/docs/Web/API/MediaDevices) offered by the browser to integrate a web camera with Vue applications.

## Getting Started

### Install

Add the package using yarn or npm:
```
yarn add @dimensional-innovations/vue-camera
```
or
```
npm install --save @dimensional-innovations/vue-camera
```

### Usage

There are two pieces from this package that you'll need to get started. The useCamera method takes a deviceId and returns the MediaStream that represents the device's view. The Camera component can then be used to show this view in a responsive layout. For example:

``` vue
<script lang="ts">
import { Camera, useCamera, useCamerasList } from '@dimensional-innovations/vue-camera';
import { defineComponent, ref, watch } from 'vue';

export default defineComponent({
  components: {
    Camera
  },
  setup () {
    const devices = useCamerasList();
    const selectedDevice = ref<string>();
    const mediaStream = useCamera(selectedDevice);

    return {
      devices,
      selectedDevice,
      mediaStream
    };
  }
});
</script>

<template>
  <div style="position: relative">
    <camera :mediaStream="mediaStream" />
    <div style="position: absolute; top: 0px; right: 0px;">
      <select v-model="selectedDevice">
        <option :value="undefined">
          Select a camera
        </option>
        <option v-for="device in devices" :key="device.deviceId" :value="device.deviceId">
          {{ device.label }}
        </option>
      </select>
    </div>
  </div>
</template>
```

Additional methods and components that can be used to manage a camera are exported from this package as well. For the complete api this package exposes see the [API](/API.md) doc.


### Troubleshooting
This package is not currently working with Vite, because of mediaStream-related issues. Consider using Vue-CLI instead.

On Tech - PB Manager, you can detect a lot of cameras. These are virtual cameras and they will not open. To detect if they will open, you can use one of the following ways:

```vue
const log = (e) => {
  console.log(e); // this returns "DOMException: Could not start video source" for PB-Manager
};

const mediaStream = useCamera(selectedDevice, {}, log);
// selectedDeviceID will change once users selected another option. But mediaStream changes once the media is loaded.
// No matter the camera source is valid or not; selectedDeviceID will exist. If it is an invalid media, mediaStream will give an error.
```

```vue
setTimeout(() => {
  if (mediaStream.value) {
    console.log('Camera was opened for the selected device.');
  } else {
    console.log('A media stream probably does not exist for this camera ... ');
    console.log('mediaStream ' + mediaStream.value); // This returns "undefined" from vue-camera package if the media stream gives an error
  }
}, 1000 * 10);
```

```vue
watch(mediaStream, (newVal, oldVal) => {
  console.log('new mediaStream');
  console.log(newVal);
  console.log('old mediaStream');
  console.log(oldVal);
  if (!mediaStream.value) {
    console.log("MediaStream changed! There's no media stream to be pulled.");
  } else if (mediaStream.value) {
    console.log('MediaStream changed! A media stream exist!');
    console.log(mediaStream.value);
  }
});
```

