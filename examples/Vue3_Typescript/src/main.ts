import { createApp } from 'vue';
import App from './App.vue';

document.addEventListener('contextmenu', (e) => {
  e.preventDefault();
  return false;
});

createApp(App)
  .mount('#app');
