import { set, useResizeObserver } from '@vueuse/core';
import first from 'lodash.first';
import { defineComponent, h, ref, watch } from 'vue';

/**
 * Component that shows a stream from a camera. The stream will automatically be
 * resized to fit within the element.
 */
export const Camera = defineComponent({
  name: 'Camera',
  props: {
    mediaStream: {
      type: MediaStream,
      required: false
    }
  },
  emits: ['load', 'error'],
  setup(props, { emit }) {
    const container = ref<HTMLDivElement>();
    const height = ref(1920);
    const width = ref(1080);
    useResizeObserver(container, (entries) => {
      const target = first(entries)?.target as HTMLDivElement;
      if (!target) return;

      set(height, target.offsetHeight);
      set(width, target.offsetWidth);
    });

    watch([() => props.mediaStream, height, width], async ([mediaStream, height, width]) => {
      if (!mediaStream) return;

      for (const track of mediaStream.getVideoTracks()) {
        await track.applyConstraints({ height, width });
      }
    });

    return () => (
      h('div', { ref: container, style: 'max-height: 100%; max-width: 100%; display: flex;' }, [
        !!props.mediaStream 
          ? h('video', { srcObject: props.mediaStream, autoplay: true, onLoadeddata: () => emit('load'), onError: ($event: any) => emit('error', $event), style: 'height: 100%; width: 100%;' }) 
          : null
      ])
    );
  }
})