import first from 'lodash.first';
import pick from 'lodash.pick';
import { CameraConstraints } from './CameraConstraints';

/**
 * Determines if the specified device is connected.
 * 
 * @param deviceId - The unique id of the device.
 * @returns A promise that resolves to true if the device is connected, or false otherwise.
 */
export async function isDeviceConnected(deviceId: string | undefined): Promise<boolean> {
  if (!deviceId) return false;

  const devices = await navigator.mediaDevices.enumerateDevices();
  return !!devices.find(device => device.deviceId === deviceId);
}

/**
 * Connects to the specified device, automatically starting the video stream.
 * 
 * @param deviceId - The unique id of the device
 * @param constraints - The set of constraints to apply to the mediaStream.
 * 
 * @return - A promise that resolves with the MediaStream for the device.
 */
export async function getMediaStream(deviceId: string, constraints: CameraConstraints = {}): Promise<MediaStream> {
  const mediaStream = await navigator.mediaDevices.getUserMedia({
    video: {
      ...pick(constraints, 'height', 'width'),
      deviceId: {
        exact: deviceId
      }
    },
    audio: false
  });

  await applyConstraints(mediaStream, constraints);

  return mediaStream;
}

/**
 * Applies the given constraints to the mediaStream.
 * 
 * @param mediaStream - The mediaStream to modify.
 * @param constraints - The constraints to apply.
 * 
 * @return - A promise that resolve once the changes have been applied.
 */
export async function applyConstraints(mediaStream: MediaStream, constraints: CameraConstraints): Promise<void> {
  /**
   * We end up applying constraints 3 times here. Only certain constraints can be applied at the same time.
   * If we attempt to apply all possible constraints at the same time, we will get an OverConstrained error
   * and no changes will occur in the mediaStream. The groups of constraints below were found through trial
   * and error.
   */

  for (const track of mediaStream.getVideoTracks()) {
    await track.applyConstraints({ ...pick(constraints, 'height', 'width')});
    await track.applyConstraints({ advanced: [{ ...pick(constraints, 'focusDistance', 'focusMode', 'zoom')} ]});
    await track.applyConstraints({ advanced: [{ ...pick(constraints, 'brightness', 'colorTemperature', 'contrast', 'saturation', 'sharpness', 'whiteBalanceMode') }]});
  }
}

/**
 * Stops the given mediaStream.
 * 
 * @param mediaStream - The mediaStream to stop.
 */
export function stopMediaStream(mediaStream: MediaStream): void {
  for (const track of mediaStream.getTracks()) {
    track.stop();
  }
}

/**
 * Converts the provided canvas element into a Blob. This wraps the canvas' toBlob method
 * in order to return a promise instead of using a callback.
 *
 * @param canvas - The canvas element to convert.
 * @param type - The mime type of the converted canvas. Defaults to 'image/png'.
 * @returns - A promise that resolves with the resulting blob.
 */
 export function toBlob(canvas: HTMLCanvasElement, type = 'image/png'): Promise<Blob> {
  return new Promise((resolve, reject) => {
    canvas.toBlob(blob => {
      if (blob) {
        resolve(blob);
      } else {
        reject(new Error('Unable to convert canvas to blob.'));
      }
    }, type);
  });
}

/**
 * Converts the provided image bitmap to a blob.
 *
 * @param image - The image to convert
 * @returns - The blob representing the converted image.
 */
 export function imageBitmapToBlob(image: ImageBitmap): Promise<Blob> {
  const canvas = document.createElement('canvas');
  canvas.height = image.height;
  canvas.width = image.width;

  const context = canvas.getContext('2d');
  if (!context) {
    throw new Error('Failed to get context from canvas');
  }

  context.drawImage(image, 0, 0, image.width, image.height);
  return toBlob(canvas);
}

/**
 * Attempts to capture a photo using the ImageCapture.takePhoto method. Will fallback to
 * using the grabFrame method if underlying device does not support the takePhoto method.
 *
 * @param mediaStream - The mediaStream used to capture a photo.
 * @returns - A promise that resolves with the Blob of the captured image.
 */
 export async function takePhoto(mediaStream: MediaStream): Promise<Blob> {
  const videoTrack = first(mediaStream.getVideoTracks());
  if (!videoTrack) {
    throw new Error('Provide mediaStream does not have a video track.');
  }

  const imageCapture = new ImageCapture(videoTrack);
  try {
    return await imageCapture.takePhoto();
  } catch (error) {
    console.warn('Failed to take photo. Falling back to frame capture.', error);

    const image = await imageCapture.grabFrame();
    return imageBitmapToBlob(image);
  }
}