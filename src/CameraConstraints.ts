
/**
 * MediaTrack constraints that are used with a camera device.
 */
export type CameraConstraints = Partial<Pick<MediaTrackConstraintSet, 'height' | 'width' | 'focusDistance' | 'focusMode' | 'zoom' | 'brightness' | 'colorTemperature' | 'contrast' | 'saturation' | 'sharpness' | 'whiteBalanceMode'>>;