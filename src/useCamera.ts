import { get, isDefined, set, useDevicesList } from '@vueuse/core';
import { computed, isReactive, onUnmounted, reactive, Ref, ref, watch } from 'vue';
import { CameraConstraints } from './CameraConstraints';
import { applyConstraints, getMediaStream, stopMediaStream } from './util';

/**
 * Gets a media stream for the specified device once the device is connected. If any constraints are provided
 * or updated, automatically applies the constraints to the stream. 
 * 
 * @param deviceId - The unique id of the device to get the MediaStream from.
 * @param constraints - Constraints, if any, to apply to the stream. If this object is reactive, new constraints 
 *                      will be applied each time the object is updated.
 * @param onError - Callback that is invoked if there an error when retrieving the media stream.
 * @returns - A ref to the MediaStream. If the device cannot be found, the ref will be undefined.
 */
export function useCamera(deviceId: Ref<string | undefined>, constraints: CameraConstraints = {}, onError?: (error: any, deviceId: string | undefined) => void): Ref<MediaStream | undefined> {
  const devices = useDevicesList({
    constraints: { video: true }
  });
  const isConnected = computed(() => !!get(devices.videoInputs).find(device => device.deviceId === get(deviceId)));
  const mediaStream = ref<MediaStream>();

  const dispose = (mediaStream: Ref<MediaStream>): void => {
    stopMediaStream(get(mediaStream));
    set(mediaStream, undefined);
  };

  watch([deviceId, isConnected], async ([deviceId, isConnected], [prevDeviceId]) => {
    try {
      if (isDefined(mediaStream) && deviceId !== prevDeviceId) {
        dispose(mediaStream);
      }
      if (isDefined(mediaStream) && !isConnected) {
        dispose(mediaStream);
      }
      if (!isDefined(mediaStream) && deviceId && isConnected) {
        set(mediaStream, await getMediaStream(deviceId, constraints));
      }
    }
    catch (error) {
      set(mediaStream, undefined);
      
      if (onError) {
        onError(error, deviceId);
      }
      else {
        throw error;
      }
    }
  }, { immediate: true });

  watch([mediaStream, isReactive(constraints) ? constraints : reactive(constraints)], async ([mediaStream, constraints]) => {
    if (!mediaStream) return;

    try {
      await applyConstraints(mediaStream, constraints);
    }
    catch (error) {
      if (onError) {
        onError(error, get(deviceId))
      }
      else {
        throw error;
      }
    }
  });
  onUnmounted(() => {
    if (isDefined(mediaStream)) {
      dispose(mediaStream);
    }
  });

  return mediaStream;
}
